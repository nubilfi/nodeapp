# NodeApp

Orenda Preliminary Test 2022

## Quickstart

```bashsession
npm i
```

## Mode

Rename `.env*` file based on your environment.

## Available commands

- Starts the containers in the background and leaves them running : `docker-compose up -d`
- Stops containers and removes containers, networks, volumes, and images : `docker-compose down`
- Run migration : `npm run prisma:migrate`
- Run the Server : `npm run start`

## API Documentation

Start the app in development mode and access through your browser at <http://localhost:3000/api-docs>

## SWC Compiler

[SWC](https://swc.rs/) is an extensible Rust-based platform for the next generation of fast developer tools.

`SWC is 20x faster than Babel on a single thread and 70x faster on four cores.`

- tsc build :: `npm run build`
- swc build :: `npm run build:swc`

Modify `.swcrc` file to your source code.
