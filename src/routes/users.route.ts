import { Router } from 'express';
import UsersController from '@controllers/users.controller';
import { RegisterDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';

class UsersRoute implements Routes {
  public path = '/api';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/register`, this.usersController.register);
    this.router.post(`${this.path}/assign`, validationMiddleware(RegisterDto, 'body'), this.usersController.assign);
    this.router.post(`${this.path}/unassign`, validationMiddleware(RegisterDto, 'body'), this.usersController.unassign);
    this.router.get(`${this.path}/task/common`, validationMiddleware(RegisterDto, 'body'), this.usersController.getTask);
  }
}

export default UsersRoute;
