import { IsString } from 'class-validator';

export class RegisterDto {
  @IsString()
  public user?: string;

  @IsString()
  public tasks?: string;
}
