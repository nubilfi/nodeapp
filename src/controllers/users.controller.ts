import { NextFunction, Request, Response } from 'express';
import { User } from '@prisma/client';
import { RegisterDto } from '@dtos/users.dto';
import userService from '@services/users.service';

class UsersController {
  public userService = new userService();

  public getTask = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const tasks: RegisterDto = req.body;
      const taskData: User[] = await this.userService.findCommonTask(tasks);

      res.status(200).json({ data: taskData, message: 'common' });
    } catch (error) {
      next(error);
    }
  };

  public register = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData: RegisterDto = req.body;
      const createUserData: User = await this.userService.registerData(userData);

      res.status(204).json({ data: createUserData, message: 'registered' });
    } catch (error) {
      next(error);
    }
  };

  public assign = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const assignData: RegisterDto = req.body;
      const createAssignData: User = await this.userService.assignData(assignData);

      res.status(204).json({ data: createAssignData, message: 'assigned' });
    } catch (error) {
      next(error);
    }
  };

  public unassign = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const assignData: RegisterDto = req.body;
      const createAssignData = await this.userService.unassignData(assignData);

      res.status(204).json({ data: createAssignData, message: 'unassigned' });
    } catch (error) {
      next(error);
    }
  };
}

export default UsersController;
