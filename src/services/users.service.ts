import { PrismaClient, User } from '@prisma/client';
import { RegisterDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';

class UserService {
  public users = new PrismaClient().user;

  public async findCommonTask(taskData: RegisterDto): Promise<User[]> {
    /* As an Admin, I should be able to list all common tasks of 2 users (e.g. if 2
      users has “Buy eggs” as common task, I should be able to see it in a list)
    • Endpoint: GET /api/tasks/common
    • Headers: Content-type: application/json
    • Success response status: 200
    TODO: Example Request
    {
      user: [“example1@email.com”, “example2@email.com”]
    }
    */
    if (isEmpty(taskData)) throw new HttpException(400, "taskData is empty");

    const findAssignData: User[] = await this.users.findMany({ where: { user: taskData.user } });
    if (!findAssignData) throw new HttpException(409, `User ${taskData.user} is not exist.`);

    const db = new PrismaClient();
    const deleteAssign = await db.$queryRaw<User[]>`
      SELECT tasks FROM User WHERE user=${taskData.user} GROUP BY tasks
    `;

    return deleteAssign;
  }

  public async registerData(userData: RegisterDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "userData is empty");
    /* As an Admin, I should be able to create user(s) so that I can assign task to them
      • Endpoint: POST /api/register
      • Headers: Content-type: application/json
      • Success response status: 204 No Content
    TODO: Should be able input an array
    Example Request
    {
      Users: [“example1@email.com”,”example2@email.com”]
    } */
    const findUser: User = await this.users.findFirst({ where: { user: userData.user } });
    if (findUser) throw new HttpException(409, `User ${userData.user} is already exists`);

    const createUserData: User = await this.users.create({ data: { ...userData }});
    return createUserData;
  }

  public async assignData(assignData: RegisterDto): Promise<User> {
    if (isEmpty(assignData)) throw new HttpException(400, "assignData is empty");
    /* As an Admin, I should be able to assign tasks to user so that they know their existing task
      • Endpoint: POST /api/assign
      • Headers: Content-type: application/json
      • Success response status: 204 No Content
    TODO: Input an array of task
    Example Request
    {
      user: “example1@email.com”,
      tasks: [“Buy eggs”, “Buy milk”]
    } */
    const createAssignData: User = await this.users.create({ data: { ...assignData }});
    return createAssignData;
  }

  public async unassignData(taskData: RegisterDto): Promise<number> {
    /* As an Admin, I should be able to remove task(s) from user so that their list are updated
    • Endpoint: POST /api/unassign
    • Headers: Content-type: application/json
    • Success response status: 204 No Content
    TODO remove tasks
    Example Request
    {
      user: “example1@email.com”,
      tasks: [“Buy eggs”]
    } */
    if (isEmpty(taskData)) throw new HttpException(400, "taskData is empty");

    const findAssignData: User[] = await this.users.findMany({ where: { user: taskData.user } });
    if (!findAssignData) throw new HttpException(409, `User ${taskData.user} is not exist.`);

    const db = new PrismaClient();
    const deleteAssign = await db.$executeRaw`UPDATE User SET tasks=${taskData.tasks} WHERE user=${taskData.user}`

    return deleteAssign;
  }
}

export default UserService;
